#!/bin/sh

set -eo pipefail

cd "$CI_PROJECT_DIR"

echo "Test Docker Plugin Entrypoint"
echo ""

echo "Variables:"
export

echo "Current directory:"
ls -al

echo "Git Status:"
git status

echo "Docker Info:"
docker info
