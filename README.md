[![Docker Repository on Quay](https://quay.io/repository/gitlab_runner_plugins_example/docker-plugin/status "Docker Repository on Quay")](https://quay.io/repository/gitlab_runner_plugins_example/docker-plugin)

This repository is built on Quay.io.

To use it with GitLab CI:
1. Install GitLab Runner, currently the [Bleeding Edge](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/install/bleeding-edge.md) is required.

1. Register GitLab Runner to use Docker Executor.

1. Add to `.gitlab-ci.yml`:

        deploy:
          stage: deploy
          image: docker-plugin
          script:
          - null-script

1. Edit `/etc/gitlab-runner/config.toml` of GitLab Runner and add to `[runners.docker]`:

        volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]

This will expose the host's Docker Engine in context of the builds
and will make it possible to use Docker Engine commands in context of builds.
